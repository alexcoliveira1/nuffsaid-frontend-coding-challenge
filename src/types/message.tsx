export enum PriorityEnum {
  Error = 1,
  Warning = 2,
  Info = 3,
}

export interface MessageObject {
  id: string,
  message: string,
  priority: PriorityEnum
}