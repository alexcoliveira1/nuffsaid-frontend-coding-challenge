import MessageListContainer from 'components/MessageListContainer'
import MessagesProvider from 'components/MessagesProvider'

export default function HomePage() {
    return (
        <MessagesProvider>
            <MessageListContainer />
        </MessagesProvider>
    )
}