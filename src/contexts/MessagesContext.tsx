import { createContext, useContext } from 'react'
import { PriorityEnum, MessageObject } from 'types/message'

export const EMPTY_MESSAGES_MAP = {
    [PriorityEnum.Error]: [],
    [PriorityEnum.Warning]: [],
    [PriorityEnum.Info]: [],
}

export type MessagesMap = Record<PriorityEnum, MessageObject[]>

export type MessagesContent = {
    messagesMap: MessagesMap,
    addNewMessage: (messageObj: Omit<MessageObject, 'id'>) => void,
    clearAllMessages: () => void,
    clearMessage: (messageObject: MessageObject) => void,
}
export const MessagesContext = createContext<MessagesContent>({
    messagesMap: EMPTY_MESSAGES_MAP,
    addNewMessage: () => {},
    clearAllMessages: () => {},
    clearMessage: () => {},
})

export const useMessagesContext = () => useContext(MessagesContext)
