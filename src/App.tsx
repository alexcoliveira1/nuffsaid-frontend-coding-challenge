import { StylesProvider } from '@material-ui/styles'
import { ThemeProvider } from 'styled-components'
import MessageListPage from 'pages/MessageListPage'
import { theme } from './theme'

export default function App() {
    return (
        <StylesProvider injectFirst>
            <ThemeProvider theme={theme}>
                <MessageListPage />
            </ThemeProvider>
        </StylesProvider>
    )
}