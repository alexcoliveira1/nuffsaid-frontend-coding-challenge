import { createTheme } from '@material-ui/core/styles';

export const theme = createTheme({
  palette: {
    primary: {
      main: '#88FCA3',
    },
    error: {
      main: '#F56236',
    },
    warning: {
      main: '#FCE788',
    },
    info: {
      main: '#88FCA3',
    },
  },
});
