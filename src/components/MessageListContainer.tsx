import { useState, useEffect, useReducer, useRef } from 'react'
import Button from '@material-ui/core/Button'
import Grid from '@material-ui/core/Grid'
import styled from 'styled-components'
import { darken } from '@material-ui/core/styles';

import Api from 'api'
import { useMessagesContext } from 'contexts/MessagesContext'
import MessageList from 'components/MessageList'
import { MessageObject, PriorityEnum } from 'types/message'

const SpacedGrid = styled(Grid)`
  margin-top: 24px;
`

const StyledButton = styled(Button)`${({ theme }) => `
  background-color: ${theme.palette.info.main};
  &:hover {
    background-color: ${darken(theme.palette.info.main, 0.2)};
  }
`}`;

export default function MessageListContainer() {
  const [_, forceUpdate] = useReducer(x => x + 1, 0);
  const {
    addNewMessage,
    clearAllMessages,
    clearMessage,
    messagesMap,
  } = useMessagesContext()
  const { current: api } = useRef(new Api({ messageCallback: addNewMessage }))

  useEffect(() => {
    api.start()
  }, [])

  const handleToggleApi = () => {
    if (api.isStarted()) {
      api.stop()
    } else {
      api.start()
    }
    forceUpdate()
  }

  const renderButtons = () => {
    const isApiStarted = api.isStarted()
    return (
      <Grid
        container
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={1}
      >
        <Grid item>
          <StyledButton
            variant="contained"
            onClick={handleToggleApi}
          >
            {isApiStarted ? 'Stop' : 'Start'}
          </StyledButton>
        </Grid>
        <Grid item>
          <StyledButton
            variant="contained"
            onClick={clearAllMessages}
          >
            Clear
          </StyledButton>
        </Grid>
      </Grid>
    )
  }

  const renderMessageLists = () => {
    return (
      <SpacedGrid container spacing={2}>
        <Grid item xs={4}>
          <MessageList
            priority={PriorityEnum.Error}
            messages={messagesMap[PriorityEnum.Error]}
            onClearMessageClick={clearMessage} />
        </Grid>
        <Grid item xs={4}>
          <MessageList
            priority={PriorityEnum.Warning}
            messages={messagesMap[PriorityEnum.Warning]}
            onClearMessageClick={clearMessage} />
        </Grid>
        <Grid item xs={4}>
          <MessageList
            priority={PriorityEnum.Info}
            messages={messagesMap[PriorityEnum.Info]}
            onClearMessageClick={clearMessage} />
        </Grid>
      </SpacedGrid>
    )
  }

  return (
    <div>
      {renderButtons()}
      {renderMessageLists()}
    </div>
  )
}
