import { ReactNode, useState, useCallback, useMemo } from 'react'
import styled from 'styled-components'
import uniqueId from 'lodash/uniqueId'
import Snackbar from '@material-ui/core/Snackbar'
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

import { MessagesContext, EMPTY_MESSAGES_MAP, MessagesMap } from 'contexts/MessagesContext'
import { MessageObject, PriorityEnum } from 'types/message'

const ErrorSnackbar = styled(Snackbar)`${({ theme }) => `
    .MuiSnackbarContent-root {
        background-color: ${theme.palette.error.main};
    }
`}`

export default function MessagesProvider({ children }: { children: ReactNode }) {
    const [errorMessageToNotify, setErrorMessageToNotify] = useState<MessageObject>()
    const closeErrorMessageNotification = () => setErrorMessageToNotify(undefined)
    const [messagesMap, setMessagesMap] = useState<MessagesMap>(EMPTY_MESSAGES_MAP)
    const addNewMessage = useCallback((messageObject: Omit<MessageObject, 'id'>) => {
        const { message, priority } = messageObject
        const newMessageObject = { id: uniqueId(), ...messageObject }
        setMessagesMap((prevMessagesMap) => ({
            ...prevMessagesMap,
            [priority]: [
                newMessageObject,
                ...prevMessagesMap[priority].slice()
            ]
        }))
        if(priority === PriorityEnum.Error) {
            setErrorMessageToNotify(newMessageObject)
        }
    }, [])
    const clearAllMessages = useCallback(() => setMessagesMap(EMPTY_MESSAGES_MAP), [])
    
    const clearMessage = useCallback((messageObject: MessageObject) => {
        setMessagesMap((prevMessagesMap) => ({
            ...prevMessagesMap,
            [messageObject.priority]: prevMessagesMap[messageObject.priority].filter((item) => item.id !== messageObject.id)
        }))
    }, [])

    const value = useMemo(() => ({
        messagesMap,
        addNewMessage,
        clearAllMessages,
        clearMessage,
    }), [
        messagesMap,
        addNewMessage,
        clearAllMessages,
        clearMessage,
    ])
    return (
        <MessagesContext.Provider value={value}>
            <ErrorSnackbar
                key={errorMessageToNotify?.id}
                anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
                open={!!errorMessageToNotify}
                autoHideDuration={2000}
                message={errorMessageToNotify?.message}
                action={(
                    <IconButton size="small" aria-label="close" color="inherit" onClick={closeErrorMessageNotification}>
                        <CloseIcon fontSize="small" />
                    </IconButton>
                )}
                onClose={closeErrorMessageNotification} />
            {children}
        </MessagesContext.Provider>
    )
}