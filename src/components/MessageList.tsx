import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardActions from '@material-ui/core/CardActions'
import styled from 'styled-components'

import { PriorityEnum, MessageObject } from 'types/message'

const List = styled.ul`
    padding-left: 0;
    > *:not(:first-child) {
        margin-top: 8px;
    }
`

const ListItem = styled.li`
    list-style-type: none;
`

const StyledCardActions = styled(CardActions)`
    justify-content: flex-end;
`

const StyledCardContent = styled(CardContent)`
    padding-bottom: 0;
`

const StyledCard = styled(Card)`${({ theme }) => `
    &.Warning {
        background-color: ${theme.palette.warning.main};
    }
    &.Error {
        background-color: ${theme.palette.error.main};
    }
    &.Info {
        background-color: ${theme.palette.info.main};
    }
`}`

interface MessageListProps {
    priority: PriorityEnum,
    messages: MessageObject[],
    onClearMessageClick: (msgObj: MessageObject) => void,
}
export default function MessageList({
    priority,
    messages,
    onClearMessageClick,
}: MessageListProps) {
    return (
        <div>
            <Typography variant="h5" color="textSecondary" component="p">{PriorityEnum[priority]} Type {priority}</Typography>
            <Typography variant="body2" color="textSecondary" component="p">Count {messages.length}</Typography>
            <List>
                {messages.map((msgObj) => (
                    <ListItem key={msgObj.id}>
                        <StyledCard className={`${PriorityEnum[priority]}`}>
                            <StyledCardContent>
                                <Typography variant="body2" color="textSecondary" component="p">
                                    {msgObj.message}
                                </Typography>
                            </StyledCardContent>
                            <StyledCardActions>
                                <Button onClick={() => onClearMessageClick(msgObj)}>Clear</Button>
                            </StyledCardActions>
                        </StyledCard>
                    </ListItem>
                ))}
            </List>
        </div>
    )
}